import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from bs4 import BeautifulSoup
import requests
from datetime import datetime, timedelta

html_doc = requests.get("https://www.wunderground.com/history/daily/br/campinas/SBKP/date/2020-6-6").text

soup = BeautifulSoup(html_doc)

soup.prettify

# 24 hour precipitation
start0 = soup.prettify().find('precip24Hour')
start = soup.prettify().find(':', start0)
finish = soup.prettify().find(',', start)
soup.prettify()[start+1:finish]

# maximum 24 hour temperature
start0 = soup.prettify().find('temperatureMax24Hour')
start = soup.prettify().find(':', start0)
finish = soup.prettify().find(',', start)
soup.prettify()[start+1:finish]

# mminimum 24 hour temperature
start0 = soup.prettify().find('temperatureMin24Hour')
start = soup.prettify().find(':', start0)
finish = soup.prettify().find(',', start)
soup.prettify()[start+1:finish]
